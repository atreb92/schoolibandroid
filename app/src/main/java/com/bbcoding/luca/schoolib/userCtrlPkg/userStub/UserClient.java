package com.bbcoding.luca.schoolib.userCtrlPkg.userStub;

import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.androidClasses.User;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * Created by luca on 01/08/17.
 */

public final class UserClient {

    private static UserClient istance = null;

    private static final Logger LOGGER = Logger.getLogger(UserClient.class.getName());
    private static UserProxyInterface stub;

    /**
     * Protected contrusctor to enable singleton pattern
     */
    private UserClient(){
        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(new ConsoleHandler());
        stub = new UserStub();
    }

    /**
     * This method is used to create a UserClient, in case it allready exsists it's reference
     * is given instead of creating a new one
     * @return
     */
    public synchronized static UserClient getIstance() {
        if(istance==null) {
            istance = new UserClient();
        }
        return istance;
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @throws IOException
     */
    public void closeUserCommunications() throws IOException {
        stub.closeUserCommunications();
        LOGGER.info("Disconnected from server\n");
        this.istance = null;
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param name Name of the new User.
     * @param surname Surname of the new User.
     * @param fiscalCode Fiscal code of the new User.
     * @param mail E-mail of the new User.
     * @param phone Phone number of the new User.
     * @param password Password of the new User.
     * @param role Role of the new User.
     * @throws IOException
     */
    public String registerUser(String name, String surname, String fiscalCode, String mail, String phone, String password, String role) throws IOException {
        User newUser = new User(name, surname, fiscalCode, mail, phone, password, role);
        return stub.registerUser(newUser);

    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode of the user is trying to log in.
     * @param password of the user is trying to log in.
     * @return
     */
    public String logIn(String fiscalCode, String password, String userType) throws IOException{
        return stub.logInUser(fiscalCode, password, userType );
    }


    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode fiscal code to be controlled.
     * @return Boolean value that indicate if the fiscal code corresponds to an active account on the server db.
     * @throws IOException
     */
    public Boolean isAccountActive(String fiscalCode) throws IOException{
        return stub.isAccountActive(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode
     * @return Boolean value that indicates if the user password is temporary
     * @throws IOException
     */
    public Boolean isPasswordTemporary(String fiscalCode) throws IOException {
        return stub.isPasswordTemporary(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode fiscal of the requested User
     * @return User object correspondig the given fiscal code
     * @throws IOException
     */
    public User getUserData(String fiscalCode) throws IOException {
        return stub.getUserData(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode of the account to be activated
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    public String setUserActive(String fiscalCode) throws IOException{
        return  stub.setUserActive(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode of the account in which the password has to be changed
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    public String changeTemporaryPassword(String fiscalCode, String newPassword) throws IOException{
        return  stub.changeTemporaryPassword(fiscalCode, newPassword );
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode of the account to be deleted
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    public String deleteUser(String fiscalCode) throws IOException {
        return  stub.deleteAccount(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the server.
     * @param fiscalCode of the account in which the password has to be reset
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    public String resetAccount(String fiscalCode) throws IOException {
        return stub.resetPassword(fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param password to check
     * @param fiscalCode corresponding the account to be checked
     * @return Boolean that indicate if the password is correct
     * @throws IOException
     */
    public Boolean checkPassword(String password, String fiscalCode) throws IOException{
        return stub.checkOldPassword(password, fiscalCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param value to replace
     * @param fiscalCode of the corresponding admin
     * @param valueID that identify the value to be changed
     * @return Confrimation or Error message
     * @throws IOException
     */
    public String modifyData(String value, String fiscalCode, Integer valueID) throws IOException {
        return stub.modifyUserData(value, fiscalCode, valueID);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param keySearch keyword for the search
     * @param searchType 1=author, 2=title, 3=category
     * @return
     * @throws IOException
     */
    public ArrayList<Book> getBooks(String keySearch, Integer searchType) throws IOException {
        return stub.getBooks(keySearch, searchType);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param fiscalCode of the user
     * @param isbnCode of the book
     * @param timestamp opening date of the reservation
     * @return
     * @throws IOException
     */
    public String reserveBook(String fiscalCode, String isbnCode, Timestamp timestamp) throws IOException {
        return stub.reserveBook(fiscalCode, isbnCode, timestamp);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param fiscalCode of the User
     * @param isbnCode of the book
     * @return
     * @throws IOException
     */
    public String deleteReservation(String fiscalCode, String isbnCode) throws IOException {
        return stub.deleteReservation(fiscalCode, isbnCode);
    }

    /**
     * This method simply invoke the right method on the UserStub in order to communicate
     * with the stub.
     * @param fiscalCode of the user
     * @return
     * @throws IOException
     */
    public ArrayList<Book> getUserReservations(String fiscalCode) throws IOException {
        return stub.getUserReservations(fiscalCode);
    }






}