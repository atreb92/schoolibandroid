package com.bbcoding.luca.schoolib;

public interface PhoneChange {
    void updatePhone(String newPhone);
}
