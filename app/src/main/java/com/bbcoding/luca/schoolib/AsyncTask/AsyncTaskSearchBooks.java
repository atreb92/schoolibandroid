package com.bbcoding.luca.schoolib.AsyncTask;

import android.os.AsyncTask;

import com.bbcoding.luca.schoolib.IProcess;
import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by luca on 14/08/17.
 */

public class AsyncTaskSearchBooks extends AsyncTask<String, Void, ArrayList<Book>> {
    private UserClient u;
    private IProcess iProcess;

    public AsyncTaskSearchBooks(IProcess iProcess) {
        this.u = new UserClientIstantiator().getUserClient();
        this.iProcess = iProcess;
    }


    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public ArrayList<Book> doInBackground(String...params) {
        String searchKey = params[0];
        Integer searchType = Integer.parseInt(params[1]);

        try {
            return u.getBooks(searchKey, searchType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onPostExecute(ArrayList<Book> result) {
        iProcess.updateAdapter(result);
        super.onPostExecute(result);
    }
}
