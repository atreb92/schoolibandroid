package com.bbcoding.luca.schoolib;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.bbcoding.luca.schoolib.Adapter.ReservationAdapter;
import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskReservations;
import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class Reservations extends Fragment implements View.OnClickListener{
    private UserClient u;
    private UserData user;
    private RecyclerView reservedBooks;
    private AsyncTaskReservations asyncTask;
    private IProcess mProcess;
    private boolean refreshInProgress;


    @Override
    public void onCreate(Bundle savedIstanceState) {
        super.onCreate(savedIstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reservations, container, false);
        user = new UserDataIstantiator().getUserData();
        reservedBooks = rootView.findViewById(R.id.reservationList);

        mProcess = new IProcess() {
            @Override
            public void updateAdapter(ArrayList<Book> result) {
                reservedBooks.setAdapter(new ReservationViewAdapter(result, getActivity()));
                reservedBooks.setLayoutManager(new LinearLayoutManager(getActivity()));

                refreshInProgress = false;
            }
        };

        asyncTask = new AsyncTaskReservations(mProcess);
        refreshInProgress = true;
        asyncTask.execute(user.getUser().getFiscalCode());


        ImageButton refreshReservations = rootView.findViewById(R.id.refreshReservations);
        refreshReservations.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onClick(View v) {
        asyncTask = new AsyncTaskReservations(mProcess);
        switch (v.getId()){
            case (R.id.refreshReservations):
                if(!refreshInProgress) {
                    refreshInProgress = true;
                    asyncTask.execute(user.getUser().getFiscalCode());
                }
                break;
        }
    }
}