package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;

/**
 * Created by luca on 01/08/17.
 */

public enum UserRole implements Serializable {

    student,
    professor,
    technician,
    librarian;

    /**
     * This is a simple method to get a UserRole object from a String.
     * @param s String that represent the UserRole object.
     * @return UserRole
     */
    public static UserRole get(String s) {
        if (s.equals(student.toString())) {
            return student;
        }
        if (s.equals(professor.toString())) {
            return professor;
        }
        if (s.equals(technician.toString())) {
            return technician;
        }
        if (s.equals(librarian.toString())) {
            return librarian;
        }

        return null;
    }
}
