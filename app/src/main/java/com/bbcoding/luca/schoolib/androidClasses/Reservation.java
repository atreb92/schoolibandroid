package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by luca on 01/08/17.
 */

public class Reservation implements Serializable {

    private Timestamp openingDate;
    private Timestamp expiringDate;
    private String fiscalCode;

    /**
     *
     * @param openingDate
     * @param expiringDate
     * @param fiscalCode
     */
    public Reservation(Timestamp openingDate, Timestamp expiringDate, String fiscalCode){
        this.openingDate = openingDate;
        this.expiringDate = expiringDate;
        this.fiscalCode = fiscalCode;
    }

    /**
     * Getter Method.
     * @return Timestamp: opening date of the reservation
     */
    public Timestamp getOpeningDate() {
        return openingDate;
    }

    /**
     * Setter method.
     * @param openingDate opening date of the reservation
     */
    public void setOpeningDate(Timestamp openingDate) {
        this.openingDate = openingDate;
    }

    public Timestamp getExpiringDate() {
        return expiringDate;
    }

    public void setExpiringDate(Timestamp expiringDate) {
        this.expiringDate = expiringDate;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }
}
