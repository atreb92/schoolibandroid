package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;

/**
 * Created by luca on 01/08/17.
 */

public enum BookCategory implements Serializable {

    engineering,
    science,
    medicine,
    math,
    music,
    sport,
    literature
}
