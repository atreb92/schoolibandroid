package com.bbcoding.luca.schoolib.userCtrlPkg.util;

import com.bbcoding.luca.schoolib.androidClasses.User;

/**
 * Created by luca on 01/08/17.
 */

public class UserData {

    private User user;

    private static UserData istance = null;

    public synchronized static UserData getIstance() {
        if(istance==null) {
            istance = new UserData();
        }
        return istance;
    }

    protected UserData(){
        this.user = null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User u) {
        this.user = u;
    }

    public void deleteData(){
        this.user = null;
    }
}
