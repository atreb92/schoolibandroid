package com.bbcoding.luca.schoolib.userCtrlPkg.userStub;

import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.androidClasses.User;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Created by luca on 01/08/17.
 */

public interface UserProxyInterface {

    public static final String IP = "atreb.ovh";
    public static final int PORT = 9998;
    public static final String end_communications_protocol = "END_COMMUNICATIONS";
    public static final String register_user_protocol = "REGISTER_USER";
    public static final String log_in_protocol = "LOGIN_USER";
    public static final String is_account_active_protocol = "IS_USER_ACTIVE";
    public static final String is_password_temporary_protocol = "IS_PASSOWORD_TEMPORARY";
    public static final String get_user_data_protocol = "GET_USER_DATA";
    public static final String set_user_active_protocol = "SET_USER_ACTIVE";
    public static final String change_temppass_protocol = "CHANGE_TEMPPASS";
    public static final String delete_account_protocol = "DELETE_ACCOUNT";
    public static final String reset_password_account_protocol  = "RESET_PASSWORD_ACCOUNT";
    public static final String check_password_protocol = "CHECK_PASSWORD";
    public static final String modify_user_data_protocol= "MODIFY_DATA";
    public static final String get_books_protocol = "GET_BOOKS";
    public static final String reserve_book_protocol = "RESERVE_BOOK";
    public static final String delete_reservation_protocol = "DELETE_RESERVATION";
    public static final String get_reservations_protocol = "GET_RESERVATIONS";

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public void closeUserCommunications() throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String registerUser(User u) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String logInUser(String fiscalCode, String password, String userType) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public Boolean isAccountActive(String fiscalCode) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public Boolean isPasswordTemporary(String fiscalCode) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public User getUserData(String fiscalCode) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String setUserActive(String fiscalCode) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String changeTemporaryPassword(String fiscalCode, String newPassword) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String deleteAccount(String fiscalCode) throws IOException;

    /**
     * This method is defined in two modules:
     * appReader - UserClient and UserStub classes.
     * serSchooLib - RealServer and UserSkeleton classes.
     * @throws IOException
     */
    public String resetPassword(String fiscalCode) throws IOException;

    /**
     * This method simply check the password for the give fiscal code.
     * @param password to check
     * @param fiscalCode corresponding the account to be checked
     * @return Boolean that indicate if the password is correct
     * @throws IOException
     */
    public Boolean checkOldPassword(String password, String fiscalCode) throws IOException;

    /**
     * This method modify a user value identified by a valueID
     * @param value to replace
     * @param fiscalCode of the corresponding admin
     * @param valueID that identify the value to be changed
     * @return
     * @throws IOException
     */
    public String modifyUserData(String value, String fiscalCode, Integer valueID) throws IOException;

    /**
     * This method lets to search for books in the library
     * @param keySearch keyword for the search
     * @param searchType 1=author, 2=title, 3=category
     * @return A list of founded books
     * @throws IOException
     */
    public ArrayList<Book> getBooks(String keySearch, Integer searchType) throws IOException;

    /**
     * This method lets to make a book reservation
     * @param fiscalCode of the user
     * @param isbnCode of the book
     * @param timestamp opening date of the reservation
     * @return Confirmation or Error message
     * @throws IOException
     */
    public String reserveBook(String fiscalCode, String isbnCode, Timestamp timestamp) throws  IOException;

    /**
     * This method lets to delete a Reservation
     * @param fiscalCode of the User
     * @param isbnCode of the book
     * @return Confirmation or error message
     * @throws IOException
     */
    public String deleteReservation(String fiscalCode, String isbnCode) throws IOException;

    /**
     * This method lets to get the list of books reserved
     * @param fiscalCode of the user
     * @return A list of reserved books
     * @throws IOException
     */
    public ArrayList<Book> getUserReservations(String fiscalCode) throws IOException;



}
