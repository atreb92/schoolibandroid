package com.bbcoding.luca.schoolib.userCtrlPkg.userStub;

/**
 * Created by luca on 11/08/17.
 */

public class UserClientIstantiator {
    private UserClient instance;

    public UserClientIstantiator() {
        instance = UserClient.getIstance();
    }

    public UserClient getUserClient() {
        return this.instance;
    }

}
