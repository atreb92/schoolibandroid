package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by luca on 01/08/17.
 */

public class User implements Serializable {

    protected String name;
    protected String surname;
    protected String fiscalCode;
    protected String mail;
    protected String phone;
    protected String password;
    protected String userRole;
    protected String activationCode;
    protected Boolean temporaryPassword;
    protected ArrayList<Reservation> reservations;
    protected ArrayList<Loan> loans;

    /**
     * This constructor simply initialises all the fields for the object.
     * @param name name of the user.
     * @param surname surname of the user.
     * @param fiscalCode fiscal code of the user.
     * @param mail mail associated to the user.
     * @param phone phone number of the user.
     * @param password password of the user.
     * @param userRole role of the user.
     */
    public User(String name,String surname,String fiscalCode, String mail, String phone, String password, String userRole){
        this.name = name;
        this.surname = surname;
        this.fiscalCode = fiscalCode;
        this.mail = mail;
        this.phone = phone;
        this.password = password;
        this.userRole = userRole;
        this.activationCode = null;
        this.reservations = new ArrayList<Reservation>();
        this.loans= new ArrayList<Loan>();
        this.temporaryPassword = false;
    }

    /**
     * This constructor simply initialises all the fields for the object
     * except for the password field initialised with a null value.
     * @param name name of the user.
     * @param surname surname of the user.
     * @param fiscalCode fiscal code of the user.
     * @param mail mail associated to the user.
     * @param phone phone number of the user.
     * @param userRole role of the user.
     */
    public User(String name,String surname,String fiscalCode, String mail, String phone, String userRole){
        this.name = name;
        this.surname = surname;
        this.fiscalCode = fiscalCode;
        this.mail = mail;
        this.phone = phone;
        this.password = null;
        this.userRole = userRole;
        this.activationCode = null;
        this.reservations = new ArrayList<Reservation>();
        this.loans= new ArrayList<Loan>();
        this.temporaryPassword = null;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter method.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getSurname() {
        return this.surname;
    }

    /**
     * Setter method.
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getFiscalCode() {
        return this.fiscalCode;
    }

    /**
     * Setter method.
     * @param fiscalCode
     */
    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * Setter method.
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * Setter method.
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Setter method.
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter method.
     * @return UserRole
     */
    public String getUserRole() {
        return this.userRole;
    }

    /**
     * Getter method.
     * @return ArrayList<Reservation>
     */
    public ArrayList<Reservation> getReservations() {
        return this.reservations;
    }

    /**
     * Add an Reservation object to the ArrayList of reservations associated to the user.
     * @param reservation
     */
    public void addReservation(Reservation reservation){
        this.reservations.add(reservation);
    }

    /**
     * Getter method.
     * @return ArrayList<Loan>
     */
    public ArrayList<Loan> getLoans() {
        return this.loans;
    }

    /**
     * Add a Loan object to the ArrayList of loans associated to the user.
     * @param loan
     */
    public void addLoan(Loan loan){
        this.loans.add(loan);
    }

    /**
     * This function is always invoked by the server software and generate a pseudo-random alfanumeric code that let
     * the user activate his acoount on the first login.
     */
    public void generateActivationCode(){
        char[] values = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 8; i++) {
            char c = values[random.nextInt(values.length)];
            sb.append(c);
        }
        String output = sb.toString();
        this.activationCode = output;
    }

    /**
     * Setter method
     * @param a activationCode of the user.
     */
    public void setActivationCode(String a){
        this.activationCode = a;
    }

    /**
     * This function is invoked by the admin server software and generate a temporary alfanumeric password that let
     * a user being registrated by an admin.
     * The user will change his password at the first log in.
     */
    public void generateTemporaryPassword(){
        char[] values = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = values[random.nextInt(values.length)];
            sb.append(c);
        }
        String output = sb.toString();
        this.password = output;
        this.temporaryPassword = true;
    }

    /**
     * This method is useful to check if the password was generated dto be temporary
     * @return Boolean value that indicates if the password is temporary
     */
    public Boolean isPasswordTemporary() {
        return temporaryPassword;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getActivationCode(){
        return this.activationCode;
    }

    @Override
    public String toString(){
        return "(" + this.name +
                ", " + this.surname +
                ", " + this.fiscalCode +
                ", " + this.mail +
                ", " + this.phone +
                ", " + this.password +
                ", " + this.userRole.toString() +
                ", " + this.activationCode +
                ")";
    }
}

