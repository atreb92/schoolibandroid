package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;

public class AsyncTaskReserveBook extends AsyncTask<String, Void, String> {
    private WeakReference<Context> weakContext;
    private UserClient u;
    private UserData user;


    public AsyncTaskReserveBook(Context context) {
        this.weakContext = new WeakReference<>(context);
        this.u = new UserClientIstantiator().getUserClient();
        this.user = new UserDataIstantiator().getUserData();
    }

    @Override
    public void onPreExecute(){
        super.onPreExecute();
    }


    @Override
    public String doInBackground(String... params) {
        String bookID = params[0];


        try {
            return u.reserveBook(user.getUser().getFiscalCode(),
                    bookID, new Timestamp(System.currentTimeMillis()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result.contains("ten active")) {
            Toast.makeText(weakContext.get(), "Reservation limit reached", Toast.LENGTH_LONG).show();
        }
        else {
            if (result.contains("duplicate key")) {
                Toast.makeText(weakContext.get(), "You already reserved this book", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
            }
        }
    }


}
