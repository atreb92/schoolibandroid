package com.bbcoding.luca.schoolib.AsyncTask;

import android.os.AsyncTask;
import android.util.Log;

import com.bbcoding.luca.schoolib.IProcess;
import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by luca on 13/08/17.
 */

public class AsyncTaskReservations extends AsyncTask<String, Void, ArrayList<Book>> {
    private UserClient u;
    private IProcess iProcess;

    public AsyncTaskReservations(IProcess iProcess) {
        this.u = new UserClientIstantiator().getUserClient();
        this.iProcess = iProcess;
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public ArrayList<Book> doInBackground(String...params) {
        String fc = params[0];
        Log.i(TAG, "Fiscal code: " + fc);

        try {

            return u.getUserReservations(fc);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public void onPostExecute(ArrayList<Book> reservedBooks) {
        iProcess.updateAdapter(reservedBooks);
        super.onPostExecute(reservedBooks);
    }
}
