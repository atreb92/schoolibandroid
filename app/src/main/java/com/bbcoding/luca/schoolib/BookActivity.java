package com.bbcoding.luca.schoolib;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskReserveBook;
import com.bbcoding.luca.schoolib.androidClasses.Book;

public class BookActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView ISBN, publicationYear, reprintYear, language, bookTitle, bookAuthor,
    bookPublisher, bookCategory;
    private Button reserveButton;
    private int year;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_info_page);
        ISBN = findViewById(R.id.labelISBN);
        publicationYear = findViewById(R.id.publicationYear);
        reprintYear = findViewById(R.id.reprintYear);
        language = findViewById(R.id.language);
        reserveButton = findViewById(R.id.reserveButton);
        bookTitle = findViewById(R.id.bookPageTitle);
        bookAuthor = findViewById(R.id.bookPageAuthor);
        bookPublisher = findViewById(R.id.bookPagePublisher);
        bookCategory = findViewById(R.id.bookPageCategory);

        ISBN.setText("ISBN: " + getIntent().getStringExtra("EXTRA_BOOK_ISBN"));
        publicationYear.setText("Pubblication Year: " + getIntent().getIntExtra(
                "EXTRA_PUBLICATION_YEAR", 0));
        year = getIntent().getIntExtra("EXTRA_REPRINT_YEAR", 0);
        if(year > 0) {
            reprintYear.setText("Reprint Year: " + getIntent().getIntExtra(
                    "EXTRA_REPRINT_YEAR", year));
        }
        else {
            reprintYear.setText("Reprint Year: No reprint");
        }

        language.setText("Language: " + getIntent().getStringExtra("EXTRA_LANGUAGE"));
        bookTitle.setText("Title: " + getIntent().getStringExtra("EXTRA_BOOK_TITLE"));
        bookAuthor.setText("Author: " + getIntent().getStringExtra("EXTRA_BOOK_AUTHOR"));
        bookPublisher.setText("Publisher: " + getIntent().getStringExtra("EXTRA_BOOK_PUB"));
        bookCategory.setText("Category: " + getIntent().getStringExtra(
                "EXTRA_BOOK_CATEGORY"));

        reserveButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        AsyncTaskReserveBook asyncTaskReserveBook = new AsyncTaskReserveBook(getApplicationContext());
        switch (v.getId()) {
            case (R.id.reserveButton):
                    asyncTaskReserveBook.execute(getIntent().getStringExtra("EXTRA_BOOK_ISBN"));
                }
    }

}
