package com.bbcoding.luca.schoolib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskRegistrationActivity;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    public static UserClient u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("SchooLib");

        String fc = getIntent().getStringExtra("fiscalCode");
        String pw = getIntent().getStringExtra("password");

        if (fc != null) {
            EditText userNameTextBox = findViewById(R.id.userName);
            userNameTextBox.setText(fc);
        }
        if (pw != null) {
            EditText passwordTextBox = findViewById(R.id.password);
            passwordTextBox.setText(pw);
        }

        Button register = findViewById(R.id.register);
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.register:
                EditText nameTextBox = findViewById(R.id.name);
                String name = nameTextBox.getText().toString();
                EditText surnameTextBox = findViewById(R.id.surname);
                String surname = surnameTextBox.getText().toString();
                EditText emailTextBox = findViewById(R.id.email);
                String email = emailTextBox.getText().toString();
                EditText userNameTextBox = findViewById(R.id.userName);
                String userName = userNameTextBox.getText().toString();
                EditText passwordTextBox = findViewById(R.id.password);
                String password = passwordTextBox.getText().toString();
                EditText password2TextBox = findViewById(R.id.passwordConfirmation);
                String password2 = password2TextBox.getText().toString();
                EditText phoneTextBox = findViewById(R.id.phoneNumber);
                String phoneNumber = phoneTextBox.getText().toString();

                if (password.length() >= 8) {
                    if (!password.equals(password2)) {
                        Toast.makeText(getApplicationContext(), "ERROR: Password don't match",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        AsyncTaskRegistrationActivity asyncTask =
                                new AsyncTaskRegistrationActivity(getApplicationContext());
                        asyncTask.execute(name, surname, userName, email, phoneNumber, password,
                                "student-3B");

                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"ERROR: Password must be at least " +
                            "of 8 characters", Toast.LENGTH_SHORT).show();
                break;

        }
    }
}
