package com.bbcoding.luca.schoolib;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskDeleteReservation;
import com.bbcoding.luca.schoolib.androidClasses.Book;

import java.util.ArrayList;

public class ReservationViewAdapter extends RecyclerView.Adapter<ReservationViewAdapter.ReservationViewHolder>{

    private ArrayList<Book> books;
    private Context context;

    public ReservationViewAdapter(ArrayList<Book> books, Context c) {
        this.books = books;
        this.context = c;
    }

    @Override
    public ReservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        ReservationViewHolder holder = new ReservationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ReservationViewHolder holder, final int position) {
        holder.bookTitle.setText(books.get(position).getTitle());
        holder.openingDate.setText(books.get(position).getAuthorToString());
        holder.expireDate.setText("ISBN: " + books.get(position).getISBN());

        holder.bookCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle("Delete Reservation");
                alertDialog.setMessage("Do you want to delete your reservation for " + books.get(position).getTitle() + "? This action cannot be undon.");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                AsyncTaskDeleteReservation deleteReservation = new AsyncTaskDeleteReservation(context);
                                deleteReservation.execute(books.get(position).getISBN());
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ReservationViewHolder extends RecyclerView.ViewHolder{

        TextView bookTitle, openingDate, expireDate;
        CardView bookCard;

        public ReservationViewHolder(View v){
            super(v);
            bookTitle = v.findViewById(R.id.bookTitle);
            openingDate = v.findViewById(R.id.bookAuthor);
            expireDate = v.findViewById(R.id.bookISBN);

            bookCard = v.findViewById(R.id.book_layout);
        }
    }

}
