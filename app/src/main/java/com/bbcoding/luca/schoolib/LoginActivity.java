package com.bbcoding.luca.schoolib;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskLoginActivity;
import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskResetPassword;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static UserClient u;
    public static UserData user;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 1500);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button login = findViewById(R.id.loginUser);
        login.setOnClickListener(this);
        Button register = findViewById(R.id.register);
        register.setOnClickListener(this);
        Button passwdReset = findViewById(R.id.passwordReset);
        passwdReset.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("SchooLib");

    }


    @Override
    public void onClick(View v) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        EditText userNameTextBox = findViewById(R.id.userName);
        String userName = userNameTextBox.getText().toString();
        EditText passwordTextBox = findViewById(R.id.password);
        String password = passwordTextBox.getText().toString();

        switch (v.getId()) {
            case R.id.loginUser:

                AsyncTaskLoginActivity a = new AsyncTaskLoginActivity(context, u, user);
                if (userName.length() == 16) {
                        a.execute(userName, password);
                }
                else  {
                    Toast.makeText(context, "ERROR: Invalid Fiscal Code", duration).show();
                }
                break;
            case R.id.register:
                Intent registerUser = new Intent(this, RegistrationActivity.class);
                if(userName.length() == 16) {
                    registerUser.putExtra("fiscalCode", userName);
                    if (password.length() >= 8)
                        registerUser.putExtra("password", password);
                }
                startActivity(registerUser);
                break;
            case R.id.passwordReset:
                AsyncTaskResetPassword asyncTaskReset = new AsyncTaskResetPassword(context, u);
                if (userName.length() == 16) {
                    asyncTaskReset.execute(userName);
                }
                else  {
                    Toast.makeText(context, "ERROR: Invalid Fiscal Code", duration).show();
                }
                break;
        }

    }
}
