package com.bbcoding.luca.schoolib;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskActivateAccount;
import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskModifyUserInfo;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

public class Profile extends Fragment implements  View.OnClickListener{
    private UserData user;
    private TextView name, surname, fiscalCode, email, phone;
    private Button changeEmail, changePassword, changePhone, deleteAccount;
    private PhoneChange phoneChange;


    @Override
    public void onCreate(Bundle savedIstanceState) {
        super.onCreate(savedIstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        user = new UserDataIstantiator().getUserData();

        name = rootView.findViewById(R.id.name);
        surname = rootView.findViewById(R.id.surname);
        fiscalCode = rootView.findViewById(R.id.fiscalCode);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phoneN);

        changeEmail = rootView.findViewById(R.id.modifyEmail);
        changePassword = rootView.findViewById(R.id.modifyPassword);
        changePhone = rootView.findViewById(R.id.modifyPhone);
        deleteAccount = rootView.findViewById(R.id.delete_Account);

        changeEmail.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        changePhone.setOnClickListener(this);
        deleteAccount.setOnClickListener(this);


        name.setText("Name: " + user.getUser().getName());
        surname.setText("Surname: " + user.getUser().getSurname());
        fiscalCode.setText("FC: " + user.getUser().getFiscalCode());
        email.setText("Email: " + user.getUser().getMail());
        phone.setText("Phone number: " + user.getUser().getPhone());

        phoneChange = new PhoneChange() {
            @Override
            public void updatePhone(String newPhone) {
                phone.setText("Phone number: " + newPhone);
            }
        };


        return rootView;

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case(R.id.modifyEmail): {
                final View view = getLayoutInflater().inflate(R.layout.alert_dialog_modify_email, null);
                final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
                alert.setTitle("Modify Email");

                final EditText newEmail = view.findViewById(R.id.newEmail);
                final EditText password = view.findViewById(R.id.password);

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "Modify", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String mail = newEmail.getText().toString();
                        String userPassword = password.getText().toString();

                        AsyncTaskModifyUserInfo async = new AsyncTaskModifyUserInfo(getActivity(), user);
                        async.execute(mail, "1", userPassword);
                    }
                });

                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert.setView(view);
                alert.show();


                break;
            }
            case (R.id.modifyPassword): {
                final View view = getLayoutInflater().inflate(R.layout.alert_dialog_modify_password, null);
                final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
                alert.setTitle("Modify Email");

                final EditText oldPassword = view.findViewById(R.id.oldPassword);
                final EditText newPassword = view.findViewById(R.id.newPassword);
                final EditText confirmPassword = view.findViewById(R.id.repeatPassword);

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "Modify", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String old = oldPassword.getText().toString();
                        String new1 = newPassword.getText().toString();
                        String new2 = confirmPassword.getText().toString();
                        if(new1.equals(new2)) {
                            AsyncTaskModifyUserInfo async = new AsyncTaskModifyUserInfo(getActivity(), user);
                            async.execute(new1, "2", old);
                        }
                        else{
                            Toast.makeText(getActivity(), "Passwords don't match", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert.setView(view);
                alert.show();


                break;
            }
            case (R.id.modifyPhone): {
                final View view = getLayoutInflater().inflate(R.layout.alert_dialog_modify_phone, null);
                final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
                alert.setTitle("Modify Email");

                final EditText newPhone = view.findViewById(R.id.newPhone);
                final EditText password = view.findViewById(R.id.passwordPhone);


                alert.setButton(AlertDialog.BUTTON_POSITIVE, "Modify", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String phone = newPhone.getText().toString();
                        String pass = password.getText().toString();


                        AsyncTaskModifyUserInfo async = new AsyncTaskModifyUserInfo(getActivity(), user, phoneChange);
                        async.execute(phone, "3", pass);

                    }
                });

                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert.setView(view);
                alert.show();

                break;
            }
            case (R.id.delete_Account): {
                final AsyncTaskActivateAccount asynctask =
                        new AsyncTaskActivateAccount(getActivity());

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Delete Account?");
                alertDialog.setMessage("Do you really want to delete your account? This operation cannot be undon.");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                asynctask.execute("DELETE", user.getUser().getFiscalCode());
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                break;
            }
        }

    }
}