package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.ActivateAccount;
import com.bbcoding.luca.schoolib.ChangeTempPassword;
import com.bbcoding.luca.schoolib.MainMenu;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by luca on 03/08/17.
 */

public class AsyncTaskLoginActivity extends AsyncTask<String, Void, Object[]> {
    private WeakReference<Context> weakContext;
    private UserClient u;
    private UserData user;

    public AsyncTaskLoginActivity(Context context, UserClient u, UserData user) {
        this.weakContext = new WeakReference<>(context);
        this.u = u;
        this.user = user;

    }

    @Override
    public void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    public Object[] doInBackground(String... params) {
        String un = params[0];
        String pw = params[1];
        u = new UserClientIstantiator().getUserClient();
        user = new UserDataIstantiator().getUserData();
        Object[] output = new Object[3];
        try {

            output[0] = u.logIn(un, pw, "U");
            if (output[0].toString().contains("user confirmed")) {
                output[1] = u.isAccountActive(un);
                output[2] = u.isPasswordTemporary(un);
                user.setUser(u.getUserData(un));
            }
            return output;
        } catch (IOException e) {
            e.printStackTrace();
            output = new Object[3];
            output[0] = "ERROR: " + e.toString();
        }
        return output;
    }
    @Override
    protected void onPostExecute(Object[] output) {

        super.onPostExecute(output);
        String result = (String) output[0];
        if (result.contains("user confirmed")) {
            result = "Login successful";
            Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
            boolean accountActive = (boolean) output[1];
            boolean passTemp = (boolean) output[2];
            if (!accountActive){
                //istanzio activity per attivare l'account
                Intent activateAccount = new Intent(weakContext.get(), ActivateAccount.class);
                activateAccount.setFlags(activateAccount.getFlags() |
                        Intent.FLAG_ACTIVITY_NO_HISTORY);
                weakContext.get().startActivity(activateAccount);
            }
            if ((passTemp)&(accountActive)) {
                //istanzio activity per il cambio password
                Intent changeTempPassword = new Intent(weakContext.get(), ChangeTempPassword.class);
                changeTempPassword.setFlags(changeTempPassword.getFlags() |
                        Intent.FLAG_ACTIVITY_NO_HISTORY);
                weakContext.get().startActivity(changeTempPassword);

            }
            if ((!passTemp)&(accountActive)) {
                //istanzio il profilo utente
                Intent mainMenu = new Intent(weakContext.get(), MainMenu.class);
                mainMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                weakContext.get().startActivity(mainMenu);

            }
        }
        else {
            Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
        }

    }
}
