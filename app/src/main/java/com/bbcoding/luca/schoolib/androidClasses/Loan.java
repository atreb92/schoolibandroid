package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by luca on 01/08/17.
 */

public class Loan implements Serializable {

    private String fiscalCode;
    private Timestamp openingDate;
    private int daysLeft;
    private Timestamp closingDate;

    /**
     * Constructor that simply initialises all fileds for an object.
     * @param openingDate opening dat eof the loan.
     */
    public Loan(String fiscalCode, Timestamp openingDate){
        this.openingDate = openingDate;
        this.closingDate = null;
        calculateDaysLeft();
    }

    public Loan(String fiscalCode, Timestamp openingDate, Timestamp closingDate){
        this.openingDate = openingDate;
        this.closingDate = closingDate;
    }

    /**
     * Getter method.
     * @return Timestamp
     */
    public Timestamp getOpeningDate() {
        return openingDate;
    }

    /**
     * Setter method.
     * @param openingDate
     */
    public void setOpeningDate(Timestamp openingDate) {
        this.openingDate = openingDate;
    }

    /**
     * Getter method.
     * @return int
     */
    public int getDaysLeft() {
        return daysLeft;
    }

    /**
     * Setter method.
     * @param daysLeft
     */
    public void decreaseDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft - 1;
    }

    /**
     * Getter method.
     * @return TimeStamp
     */
    public Timestamp getClosingDate() {
        return closingDate;
    }


    /**
     * This method calculate the days left for the Loan.
     */
    public void calculateDaysLeft(){
        Calendar today = Calendar.getInstance();
        Calendar closing = Calendar.getInstance();
        closing.setTime(this.openingDate);
        closing.add(Calendar.DAY_OF_WEEK, 30);
        long difference = closing.getTimeInMillis() - today.getTimeInMillis();
        this.daysLeft = (int) (difference/ (1000*60*60*24));
    }

}
