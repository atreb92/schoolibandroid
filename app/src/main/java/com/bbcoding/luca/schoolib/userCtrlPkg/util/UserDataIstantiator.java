package com.bbcoding.luca.schoolib.userCtrlPkg.util;

/**
 * Created by luca on 11/08/17.
 */

public class UserDataIstantiator {
    private UserData instance;

    public UserDataIstantiator() {
        instance = UserData.getIstance();
    }

    public UserData getUserData() {
        return this.instance;
    }

}
