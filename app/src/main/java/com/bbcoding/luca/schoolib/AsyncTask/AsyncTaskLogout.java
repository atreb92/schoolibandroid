package com.bbcoding.luca.schoolib.AsyncTask;

import android.os.AsyncTask;

import com.bbcoding.luca.schoolib.androidClasses.User;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

import java.io.IOException;

/**
 * Created by luca on 13/08/17.
 */

public class AsyncTaskLogout extends AsyncTask<Void, Void, Void> {
    private UserData user;
    private UserClient u;

    public AsyncTaskLogout() {
        this.u = new UserClientIstantiator().getUserClient();
        this.user = new UserDataIstantiator().getUserData();

    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public Void doInBackground(Void...params) {
        try {
            u.closeUserCommunications();
            user.deleteData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(Void v) {
        super.onPostExecute(v);
    }
}
