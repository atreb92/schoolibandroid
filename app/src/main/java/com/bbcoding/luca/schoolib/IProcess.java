package com.bbcoding.luca.schoolib;

import com.bbcoding.luca.schoolib.androidClasses.Book;

import java.util.ArrayList;

public interface IProcess {
    void updateAdapter(ArrayList<Book> result);
}
