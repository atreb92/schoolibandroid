package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by luca on 12/08/17.
 */

public class AsyncTaskResetPassword extends AsyncTask<String, Void, String> {
    private UserClient u;
    private WeakReference<Context> weakContext;

    public AsyncTaskResetPassword(Context c, UserClient u) {
        this.weakContext = new WeakReference<>(c);
        this.u = u;
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public String doInBackground(String...params) {
        u = new UserClientIstantiator().getUserClient();
        String fc = params[0];
        String output;

        try {
            return u.resetAccount(fc);
        } catch (IOException e) {
            e.printStackTrace();
            output = "ERROR: " + e.toString();
        }
        return output;

    }

    @Override
    public void onPostExecute(String result) {
        super.onPostExecute(result);
        Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
    }
}
