package com.bbcoding.luca.schoolib.userCtrlPkg.userStub;



import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.androidClasses.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

/**
 * Created by luca on 01/08/17.
 */

public class UserStub implements UserProxyInterface {

    private static final Logger LOGGER = Logger.getLogger(UserStub.class.getName());
    private Socket socket;
    private ObjectOutputStream outStream;
    private ObjectInputStream inStream;

    /**
     * This constructor initialise a new socket and input/output stream with it.
     */
    public UserStub() {
        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(new ConsoleHandler());

        try{

            InetAddress addr = InetAddress.getByName(UserProxyInterface.IP);

            socket = new Socket(addr, UserProxyInterface.PORT);
            outStream = new ObjectOutputStream(socket.getOutputStream());
            inStream = new ObjectInputStream(socket.getInputStream());
            LOGGER.info("Connected to server\n");

        }catch(Exception e){
            LOGGER.info(e.toString());

        }

    }

    /**
     * This method send a string protocol in order to end communications with the server.
     */
    @Override
    public void closeUserCommunications() {
        try{
            outStream.writeUTF(end_communications_protocol);
            outStream.flush();
            LOGGER.info("Protocol sended: " + end_communications_protocol+ "\n");
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * This method simply invoke the register private method passing as a parameter the right String
     * protocol for the server.
     * @param u
     * @throws IOException
     */
    public String registerUser(User u) throws IOException{

        String message = null;

        try{
            outStream.writeUTF(register_user_protocol);
            outStream.flush();
            LOGGER.info("Protocol sended: " + register_user_protocol + "\n");
            outStream.writeObject(u);
            outStream.flush();
            message = inStream.readUTF();
        }catch(IOException e){
            e.printStackTrace();
            return "ERROR: " + e.getMessage();
        }

        return message;
    }

    public String logInUser(String fiscalCode, String password, String userType) throws IOException {
        String message = null;
        try{
            outStream.writeUTF(log_in_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            outStream.writeUTF(password);
            outStream.flush();
            outStream.writeUTF(userType);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }


    /**
     * This method invoke verifyFiscalCode method passing as a parameter the right String protocol.
     * @param fiscalCode fiscal code to be controlled.
     * @return Boolean value that indicate if the fiscal code corresponds to an active account on the server db.
     * @throws IOException
     */
    @Override
    public Boolean isAccountActive(String fiscalCode) throws IOException {
        return verifyFiscalCode(fiscalCode, is_account_active_protocol);
    }

    /**
     * This method invoke verifyFiscalCode method passing as a parameter the right String protocol.
     * @param fiscalCode
     * @return Boolean value that indicates if the user password is temporary
     * @throws IOException
     */
    @Override
    public Boolean isPasswordTemporary(String fiscalCode) throws IOException {
        return verifyFiscalCode(fiscalCode, is_password_temporary_protocol);
    }

    /**
     * This private method is useful to verify on the client if the fiscal code exists on the on the server db
     * or if the corresponding account is active.
     * @param fiscalCode fiscal code to be controlled.
     * @param protocol Control process to be executed.
     * @return
     */
    private Boolean verifyFiscalCode(String fiscalCode, String protocol){
        Boolean result = null;
        try{
            outStream.writeUTF(protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            result = inStream.readBoolean();

        }catch(IOException e){
            e.printStackTrace();
        }

        return result;
    }

    /**
     * This method communicate with the server in order to obtain the user object
     * corresponding the given fiscal code
     * @param fiscalCode of the user we want to get the data
     * @return User object containing user data
     * @throws IOException
     */
    @Override
    public User getUserData(String fiscalCode) throws IOException {
        User u = null;
        try{
            outStream.writeUTF(get_user_data_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            u = (User) inStream.readObject();

        }catch (Exception e){
            e.printStackTrace();
        }
        return u;
    }

    /**
     * This method communicate to server in order to activate the account corresponding the fiscal code.
     * @param fiscalCode of the account to be activated
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    @Override
    public String setUserActive(String fiscalCode) throws IOException {
        String message = null;
        try{
            outStream.writeUTF(set_user_active_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    /**
     * This method is used to communicate with the server in order to change the temporary password
     * @param fiscalCode of the account in which the password has to be changed
     * @return Confirmation or error message for the user
     * @throws IOException
     */
    @Override
    public String changeTemporaryPassword(String fiscalCode, String newPassword) throws IOException {
        String message = null;
        try{
            outStream.writeUTF(change_temppass_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            outStream.writeUTF(newPassword);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;

    }

    @Override
    public String deleteAccount(String fiscalCode) throws IOException {
        String message = null;
        try{
            outStream.writeUTF(delete_account_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    @Override
    public String resetPassword(String fiscalCode) throws IOException {
        String message = null;
        try{
            outStream.writeUTF(reset_password_account_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    /**
     * This method simply invoke the right method on the AdminStub in order to communicate
     * with the server.
     * @param password to check
     * @param fiscalCode corresponding the account to be checked
     * @return Boolean that indicate if the password is correct
     * @throws IOException
     */
    public Boolean checkOldPassword(String password, String fiscalCode) throws IOException {

        Boolean res;

        try{
            outStream.writeUTF(check_password_protocol);
            outStream.flush();
            outStream.writeUTF(password);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            res = inStream.readBoolean();

        }catch(IOException e){
            e.printStackTrace();
            return false;
        }

        return res;

    }

    /**
     * This method simply invoke the right method on the AdminStub in order to communicate
     * with the server.
     * @param value to replace
     * @param fiscalCode of the corresponding admin
     * @param valueID that identify the value to be changed
     * @return
     * @throws IOException
     */
    @Override
    public String modifyUserData(String value, String fiscalCode, Integer valueID) throws IOException {
        String message = null;

        try{
            outStream.writeUTF(modify_user_data_protocol);
            outStream.flush();
            outStream.writeUTF(value);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            outStream.writeInt(valueID);
            outStream.flush();
            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    /**
     *
     * @param keySearch keyword for the search
     * @param searchType 1=author, 2=title, 3=category
     * @return
     * @throws IOException
     */
    @Override
    public ArrayList<Book> getBooks(String keySearch, Integer searchType) throws IOException {

        ArrayList<Book> books = null;

        try{
            outStream.writeUTF(get_books_protocol);
            outStream.flush();
            outStream.writeUTF(keySearch);
            outStream.flush();
            outStream.writeInt(searchType);
            outStream.flush();
            books = (ArrayList<Book>) inStream.readObject();

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

        return books;
    }

    /**
     *
     * @param fiscalCode of the user
     * @param isbnCode of the book
     * @param timestamp opening date of the reservation
     * @return
     * @throws IOException
     */
    @Override
    public String reserveBook(String fiscalCode, String isbnCode, Timestamp timestamp) throws IOException {
        String message = null;

        try{
            outStream.writeUTF(reserve_book_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            outStream.writeUTF(isbnCode);
            outStream.flush();
            outStream.writeObject(timestamp);
            outStream.flush();

            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    /**
     *
     * @param fiscalCode of the User
     * @param isbnCode of the book
     * @return
     * @throws IOException
     */
    @Override
    public String deleteReservation(String fiscalCode, String isbnCode) throws IOException {
        String message = null;

        try{
            outStream.writeUTF(delete_reservation_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            outStream.writeUTF(isbnCode);
            outStream.flush();

            message = inStream.readUTF();

        }catch(IOException e){
            e.printStackTrace();
            return "ERROR:\n" + e.getMessage();
        }

        return message;
    }

    /**
     *
     * @param fiscalCode of the user
     * @return
     * @throws IOException
     */
    @Override
    public ArrayList<Book> getUserReservations(String fiscalCode) throws IOException {

        ArrayList<Book> books = null;

        try{
            outStream.writeUTF(get_reservations_protocol);
            outStream.flush();
            outStream.writeUTF(fiscalCode);
            outStream.flush();
            books = (ArrayList<Book>) inStream.readObject();

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

        return books;
    }


}

