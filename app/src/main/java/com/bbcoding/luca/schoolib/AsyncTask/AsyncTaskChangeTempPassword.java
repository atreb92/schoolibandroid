package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.MainMenu;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by luca on 12/08/17.
 */

public class AsyncTaskChangeTempPassword extends AsyncTask<String, Void, String> {
    private UserClient u;
    private WeakReference<Context> weakContext;

    public AsyncTaskChangeTempPassword(Context c) {
        this.weakContext = new WeakReference<>(c);
        this.u = new UserClientIstantiator().getUserClient();

    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public String doInBackground(String...params) {
        String fc = params[0];
        String password = params[1];
        String output;
        try {
            return u.changeTemporaryPassword(fc, password);
        } catch (IOException e) {
            e.printStackTrace();
            output = "ERROR: " + e.toString();
        }
        return output;

    }

    @Override
    public void onPostExecute(String result) {
        super.onPostExecute(result);
        Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();

        if(!result.contains("ERROR")) {
            Intent mainMenu = new Intent(weakContext.get(), MainMenu.class);
            mainMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            weakContext.get().startActivity(mainMenu);
        }
    }
}
