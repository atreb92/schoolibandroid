package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.IProcess;
import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class AsyncTaskDeleteReservation extends AsyncTask<String, Void, String> {
    private UserData user;
    private UserClient u;
    private WeakReference<Context> weakContext;

    public AsyncTaskDeleteReservation(Context mContext) {
        this.weakContext = new WeakReference(mContext);
        this.u = new UserClientIstantiator().getUserClient();
        this.user = new UserDataIstantiator().getUserData();
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public String doInBackground(String...params) {
        String isbn = params[0];

        try {
            return u.deleteReservation(user.getUser().getFiscalCode(), isbn);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }


    @Override
    public void onPostExecute(String result) {
        Toast.makeText(weakContext.get(), result, Toast.LENGTH_SHORT).show();
        super.onPostExecute(result);
    }

}
