package com.bbcoding.luca.schoolib.androidClasses;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by luca on 01/08/17.
 */

public class Book implements Serializable {

    private static final long serialVersionUID = -6633376961511508469L;

    private String ISBN;
    private String title;
    private String publisher;
    private Integer publicationYear;
    private Integer reprintYear;
    private String language;
    private String bookshelf;
    private String category;
    private Integer numberOfReservations;
    private ArrayList<String> authors;
    private ArrayList<Reservation> reservations;
    private Loan activeLoan;
    private ArrayList<Loan> historyLoans;

    /**
     * Constructor that simply intialize all fields for an object.
     * @param ISBN isbn of the book.
     * @param title title of the book.
     * @param publisher publisher of the book.
     * @param publicationYear publication year of the book.
     * @param rePrintYear rePrintYear of the book.
     * @param language Laungue in which the book is written.
     * @param bookShelf bookShelf where the book is preserved.
     * @param category Which genre the book belongs to.
     * @param authors Authors of the book.
     */
    public Book(String ISBN, String title, String publisher ,Integer publicationYear, Integer rePrintYear, String language, String bookShelf, String category, ArrayList<String> authors){
        this.ISBN = ISBN;
        this.title = title;
        this.publisher = publisher;
        this.publicationYear = publicationYear;
        this.reprintYear = rePrintYear;
        this.language = language;
        this.bookshelf = bookShelf;
        this.category = category;
        this.authors= authors;
        this.numberOfReservations = null;
        this.reservations = new ArrayList<Reservation>();
        this.activeLoan = null;
        this.historyLoans = new ArrayList<Loan>();
    }

    /**
     * Constructor that simply intialize all fields for an object, with re-print year as null.
     * @param ISBN isbn of the book.
     * @param title title of the book.
     * @param publisher publisher of the book.
     * @param publicationYear publication year of the book.
     * @param language Laungue in which the book is written.
     * @param bookShelf bookShelf where the book is preserved.
     * @param category Which genre the book belongs to.
     * @param authors Authors of the book.
     */
    public Book(String ISBN, String title, String publisher ,Integer publicationYear, String language, String bookShelf, String category, ArrayList<String> authors){
        this.ISBN = ISBN;
        this.title = title;
        this.publisher = publisher;
        this.publicationYear = publicationYear;
        this.reprintYear = null;
        this.language = language;
        this.bookshelf = bookShelf;
        this.category = category;
        this.authors= authors;
        this.numberOfReservations = null;
        this.reservations = new ArrayList<Reservation>();
        this.activeLoan = null;
        this.historyLoans = new ArrayList<Loan>();
    }


    public String toStringWithReservations(){
        return this.title + ", ISBN: " + this.getISBN() + ", Reservations: " + this.numberOfReservations;
    }

    public String toStringActiveLoan(){
        return this.title + ", ISBN: " + this.getISBN() + ", Opening date: " + this.activeLoan.getOpeningDate() + ", Days left: " + this.activeLoan.getDaysLeft();
    }

    public ArrayList<String> toStringHistory(){
        ArrayList<String> strings = new ArrayList<String>();
        for(Loan l : historyLoans){
            strings.add(this.title + ", ISBN: " + this.getISBN() + ", Closing date: " + l.getClosingDate());
        }
        return strings;
    }

    @Override
    public String toString(){
        return this.title + ", ISBN: " + this.getISBN();
    }

    /**
     * Getter method.
     * @return String
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * Setter method .
     * @param ISBN
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method.
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Setter method.
     * @param publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * Getter method.
     * @return int
     */
    public Integer getPublicationYear() {
        return publicationYear;
    }

    /**
     * Setter method.
     * @param publicationYear
     */
    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    /**
     * Getter method.
     * @return int
     */
    public Integer getRePrintYear() {
        return reprintYear;
    }

    /**
     * Setter method.
     * @param rePrintYear
     */
    public void setRePrintYear(Integer rePrintYear) {
        this.reprintYear = rePrintYear;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Setter method.
     * @param language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getBookShelf() {
        return bookshelf;
    }

    /**
     * Setter method.
     * @param bookShelf
     */
    public void setBookShelf(String bookShelf) {
        this.bookshelf = bookShelf;
    }

    /**
     * Getter method.
     * @return String
     */
    public String getCategory() {
        return category;
    }

    /**
     * Setter method.
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Getter method.
     * @return String
     */
    public ArrayList<String> getAuthors() {
        return authors;
    }

    /**
     * Setter method.
     * @param authors
     */
    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    /**
     * getter method
     * @return
     */
    public Integer getNumberOfReservations() {
        return numberOfReservations;
    }

    /**
     * setter method
     * @param value
     */
    public void setNumberOfReservations(Integer value) {
        this.numberOfReservations = value;
    }

    /**
     * getter method
     * @return
     */
    public ArrayList<Reservation> getReservations() {
        return reservations;
    }

    /**
     * setter method
     * @param reservation
     */
    public void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }

    /**
     * getter method
     * @return
     */
    public Loan getActiveLoan() {
        return activeLoan;
    }

    /**
     * setter method
     * @param l
     */
    public void setActiveLoan(Loan l) {
        this.activeLoan = l;
    }

    /**
     * getter method
     * @return
     */
    public ArrayList<Loan> getHistoryLoans() {
        return historyLoans;
    }

    /**
     * Setter method
     * @param historyLoan
     */
    public void addHistoryLoan(Loan historyLoan) {
        this.historyLoans.add(historyLoan);
    }

    public String getAuthorToString() {
        ArrayList<String> authors = getAuthors();
        String result = "";
        for(int i = 0; i < authors.size(); i++) {
            if (i > 0) {
                result = result + ", ";
            }
            result = result + authors.get(i);
        }
        return result;
    }
}
