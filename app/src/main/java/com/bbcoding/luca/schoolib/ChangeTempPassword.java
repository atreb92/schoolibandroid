package com.bbcoding.luca.schoolib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskChangeTempPassword;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

public class ChangeTempPassword extends AppCompatActivity implements View.OnClickListener {
    private UserClient u;
    private UserData user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_temp_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("SchooLib");

        user = new UserDataIstantiator().getUserData();

        Button confirm = findViewById(R.id.confrim);
        confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        EditText passwordTextBox = findViewById(R.id.password);
        String password = passwordTextBox.getText().toString();
        EditText passwordConfirmationTextBox = findViewById(R.id.passwordConfirmation);
        String passwordConfirmation = passwordConfirmationTextBox.getText().toString();

        AsyncTaskChangeTempPassword asynctask =
                new AsyncTaskChangeTempPassword(getApplicationContext());

        switch (v.getId()) {
            case R.id.confrim:
                if (password.length() >=8) {
                    if (password.equals(passwordConfirmation)) {
                        asynctask.execute(user.getUser().getFiscalCode(), password);
                    } else
                        Toast.makeText(getApplicationContext(), "ERROR: Password don't match",
                                Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), "ERROR: Password must be at least " +
                            "of 8 characters", Toast.LENGTH_SHORT).show();
        }

    }
}
