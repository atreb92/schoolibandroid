package com.bbcoding.luca.schoolib;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskSearchBooks;
import com.bbcoding.luca.schoolib.androidClasses.Book;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;

import java.util.ArrayList;

public class LibraryTab extends Fragment implements View.OnClickListener {
    private UserClient u;
    private Spinner searchSpinner;
    private EditText searchKey;
    private boolean searchInProgress;
    private RecyclerView recyclerView;
    private IProcess mProcess;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_library, container, false);
        searchSpinner = rootView.findViewById(R.id.searchSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.search_by, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchSpinner.setAdapter(adapter);
        ImageButton searchButton = rootView.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);
        searchKey = rootView.findViewById(R.id.searchKey);
        recyclerView = rootView.findViewById(R.id.bookList);

        mProcess = new IProcess() {
            @Override
            public void updateAdapter(ArrayList<Book> result) {
                recyclerView.setAdapter(new RecyclerViewAdapter(result, getActivity()));
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                searchInProgress = false;

            }
        };
        return rootView;
    }

    @Override
    public void onClick(View v) {
        AsyncTaskSearchBooks asyncTaskSearch =
                new AsyncTaskSearchBooks(mProcess);
        String searchType = "2";
        String toSearch = searchKey.getText().toString();
        switch(searchSpinner.getSelectedItem().toString()) {
            case "Title":
                searchType = "2";
                break;
            case "Author":
                searchType = "1";
                break;
            case "Category":
                searchType = "3";
                break;
        }
        switch (v.getId()) {
            case (R.id.searchButton):
                if(!searchInProgress) {
                    searchInProgress = true;
                    asyncTaskSearch.execute(toSearch, searchType);
                }
                else {
                    Toast.makeText(getActivity(), "Search in progress, wait for it to complete", Toast.LENGTH_SHORT).show();

                }
                break;


        }
    }
}