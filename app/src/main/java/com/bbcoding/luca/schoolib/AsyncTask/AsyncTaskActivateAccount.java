package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.ChangeTempPassword;
import com.bbcoding.luca.schoolib.LoginActivity;
import com.bbcoding.luca.schoolib.MainMenu;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by luca on 11/08/17.
 */

public class AsyncTaskActivateAccount extends AsyncTask<String, Void, String[]> {
    private UserClient u;
    private WeakReference<Context> weakReference;

    public AsyncTaskActivateAccount(Context c) {
        this.u = new UserClientIstantiator().getUserClient();
        this.weakReference = new WeakReference<>(c);
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public String[] doInBackground(String...params) {
        String cmd = params[0];
        String fc = params[1];
        String output[] = new String[3];

        if (cmd.equals("ACTIVATE")) {
            try {
                output[0] = cmd;
                output[1] = u.setUserActive(fc);
                if (u.isPasswordTemporary(fc)) {
                    output[2] = "TRUE";
                }
                else
                    output[2] = "FALSE";
                return output;
            } catch (IOException e) {
                e.printStackTrace();
                output = new String[1];
                output[0] = "ERROR: " + e.toString();
            }
            return output;
        }
        if (cmd.equals("DELETE")) {
            try {
                output[0] = cmd;
                output[1] = u.deleteUser(fc);
                return output;
            } catch (IOException e) {
                e.printStackTrace();
                output = new String[1];
                output[0] = "ERROR: " + e.toString();
            }
            return output;
        }
        output = new String[1];
        output[0] = "ERROR: Command not found";
        return output;
    }

    @Override
    public void onPostExecute(String[] result) {
        super.onPostExecute(result);

        switch (result[0]) {
            case "DELETE":
                Toast.makeText(weakReference.get(), result[1], Toast.LENGTH_LONG).show();
                Intent loginActivity = new Intent(weakReference.get(), LoginActivity.class);
                loginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                weakReference.get().startActivity(loginActivity);
                break;
            case "ACTIVATE":
                Toast.makeText(weakReference.get(), result[1], Toast.LENGTH_LONG).show();
                if (!result[1].contains("ERROR")) {
                    if (result[2].contains("TRUE")) {
                        Intent changeTempPassword = new Intent(weakReference.get(), ChangeTempPassword.class);
                        changeTempPassword.setFlags(changeTempPassword.getFlags() |
                                Intent.FLAG_ACTIVITY_NO_HISTORY);
                        weakReference.get().startActivity(changeTempPassword);
                    } else {
                        Intent mainMenu = new Intent(weakReference.get(), MainMenu.class);
                        mainMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        weakReference.get().startActivity(mainMenu);
                    }
                }
                break;
            default:
                Toast.makeText(weakReference.get(), result[0], Toast.LENGTH_LONG).show();
                break;
        }


    }
}
