package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.LoginActivity;
import com.bbcoding.luca.schoolib.PhoneChange;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;

import java.io.IOException;
import java.lang.ref.WeakReference;

import static android.content.ContentValues.TAG;

public class AsyncTaskModifyUserInfo extends AsyncTask<String, Void, String> {
    private UserClient u;
    private UserData user;
    private WeakReference<Context> weakContext;
    private PhoneChange phoneChange;
    private String phone;


    public AsyncTaskModifyUserInfo(Context c, UserData user) {
        this.user = user;
        this.weakContext = new WeakReference<>(c);
        this.u = new UserClientIstantiator().getUserClient();
    }

    public AsyncTaskModifyUserInfo(Context c, UserData user, PhoneChange phoneChange) {
        this.user = user;
        this.weakContext = new WeakReference<>(c);
        this.phoneChange = phoneChange;
        this.u = new UserClientIstantiator().getUserClient();
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }


    @Override
        public String doInBackground(String...params) {
        String newValue = params[0];
        Log.i(TAG, newValue);
        Integer type = Integer.parseInt(params[1]);
        String password = params[2];
        phone = params[0];

        try {
            String out = u.logIn(user.getUser().getFiscalCode(), password, "U");
            if(out.contains("ERROR")) {
                return out;
            }
            else {
                return u.modifyData(newValue, user.getUser().getFiscalCode(), type);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }





    @Override
    public void onPostExecute(String result) {
        super.onPostExecute(result);
        Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
        Log.i(TAG, result);
        if(result.contains("disconnected")) {
                AsyncTaskLogout asyncTaskLogout = new AsyncTaskLogout();
                asyncTaskLogout.execute();
                Intent loginActivity = new Intent(weakContext.get(), LoginActivity.class);
                loginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                weakContext.get().startActivity(loginActivity);

        }
        if(result.contains("Phone number updated successfully")) {
            phoneChange.updatePhone(phone);
        }
    }
}
