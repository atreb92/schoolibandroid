package com.bbcoding.luca.schoolib;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bbcoding.luca.schoolib.androidClasses.Book;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private ArrayList<Book> books;
    private Context context;
    private Intent intent;

    public RecyclerViewAdapter(ArrayList<Book> books, Context c) {
        this.books = books;
        this.context = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        intent = new Intent(context, BookActivity.class);
        holder.bookTitle.setText(books.get(position).getTitle());
        holder.bookAuthor.setText(books.get(position).getAuthorToString());
        holder.bookISBN.setText("ISBN: " + books.get(position).getISBN());

        holder.bookCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("EXTRA_BOOK_ISBN", books.get(position).getISBN());
                intent.putExtra("EXTRA_PUBLICATION_YEAR",
                        books.get(position).getPublicationYear());
                intent.putExtra("EXTRA_REPRINT_YEAR",
                        books.get(position).getRePrintYear());
                intent.putExtra("EXTRA_LANGUAGE", books.get(position).getLanguage());
                intent.putExtra("EXTRA_BOOK_TITLE", books.get(position).getTitle());
                intent.putExtra("EXTRA_BOOK_AUTHOR",
                        books.get(position).getAuthorToString());
                intent.putExtra("EXTRA_BOOK_PUB", books.get(position).getPublisher());
                intent.putExtra("EXTRA_BOOK_CATEGORY", books.get(position).getCategory());

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView bookTitle, bookAuthor, bookISBN;
        CardView bookCard;

        public ViewHolder(View v){
            super(v);
            bookTitle = v.findViewById(R.id.bookTitle);
            bookAuthor = v.findViewById(R.id.bookAuthor);
            bookISBN = v.findViewById(R.id.bookISBN);

            bookCard = v.findViewById(R.id.book_layout);
        }
    }
}
