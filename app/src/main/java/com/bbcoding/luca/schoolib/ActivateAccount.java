package com.bbcoding.luca.schoolib;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.AsyncTask.AsyncTaskActivateAccount;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserData;
import com.bbcoding.luca.schoolib.userCtrlPkg.util.UserDataIstantiator;

public class ActivateAccount extends AppCompatActivity implements View.OnClickListener {
    private UserClient u;
    private UserData user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("SchooLib");

        user = new UserDataIstantiator().getUserData();

        Button confirmCode = findViewById(R.id.codeConfirmation);
        confirmCode.setOnClickListener(this);
        Button deleteAccount = findViewById(R.id.deleteAccounnt);
        deleteAccount.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        final String delete = "DELETE";
        final String activate = "ACTIVATE";
        final AsyncTaskActivateAccount asynctask =
                new AsyncTaskActivateAccount(getApplicationContext());

        switch(v.getId()) {
            case R.id.codeConfirmation:
                EditText codeConfirmationTextBox = findViewById(R.id.activationCode);
                String activationCode = codeConfirmationTextBox.getText().toString();
                if (user.getUser().getActivationCode().equals(activationCode)) {
                    asynctask.execute(activate, user.getUser().getFiscalCode());
                }
                else
                    Toast.makeText(getApplicationContext(),
                            "ERROR: wrong activation code", Toast.LENGTH_SHORT).show();
                break;
            case R.id.deleteAccounnt:
                new AlertDialog.Builder(this)
                        .setTitle("Delete Account?")
                        .setMessage("Do you really want to delete your account? This operation cannot be undon")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                asynctask.execute(delete, user.getUser().getFiscalCode());
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
                break;
        }

    }
}
