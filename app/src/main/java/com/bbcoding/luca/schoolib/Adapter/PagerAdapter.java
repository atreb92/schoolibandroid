package com.bbcoding.luca.schoolib.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bbcoding.luca.schoolib.LibraryTab;
import com.bbcoding.luca.schoolib.Profile;
import com.bbcoding.luca.schoolib.Reservations;

/**
 * Created by luca on 13/08/17.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new LibraryTab();

            case 1:
                return new Reservations();
            case 2:
                return new Profile();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

