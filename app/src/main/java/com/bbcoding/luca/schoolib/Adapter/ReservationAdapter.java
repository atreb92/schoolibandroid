package com.bbcoding.luca.schoolib.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bbcoding.luca.schoolib.R;
import com.bbcoding.luca.schoolib.androidClasses.Book;

import java.util.ArrayList;

/**
 * Created by luca on 14/08/17.
 */

public class ReservationAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Book> books;

    public ReservationAdapter(Context context, ArrayList<Book> books) {
        this.mContext = context;
        this.books = books;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Object getItem(int position) {
        return books.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //4
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View rowView = mInflater.inflate(R.layout.reservation_line_adapter, parent, false);

        TextView title = (TextView) rowView.findViewById(R.id.bookTitle);
        TextView isbn = (TextView) rowView.findViewById(R.id.bookISBN);
        title.setText(books.get(position).getTitle());
        isbn.setText("ISBN: " + books.get(position).getISBN());

        return rowView;
    }
}
