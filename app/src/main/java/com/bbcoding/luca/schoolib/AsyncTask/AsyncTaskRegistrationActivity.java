package com.bbcoding.luca.schoolib.AsyncTask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bbcoding.luca.schoolib.LoginActivity;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClient;
import com.bbcoding.luca.schoolib.userCtrlPkg.userStub.UserClientIstantiator;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by luca on 11/08/17.
 */

public class AsyncTaskRegistrationActivity extends AsyncTask<String, Void, String> {
    private WeakReference<Context> weakContext;
    private UserClient u;

    public AsyncTaskRegistrationActivity(Context context) {
        this.weakContext = new WeakReference<>(context);
        this.u = new UserClientIstantiator().getUserClient();
    }


    @Override
    public void onPreExecute(){
        super.onPreExecute();
    }


    @Override
    public String doInBackground(String... params) {
        String output;
        String name = params[0];
        String surname = params[1];
        String fiscalCode = params[2];
        String mail = params[3];
        String phone = params[4];
        String password = params[5];
        String role = params[6];
        System.out.println(name + " " + surname + " " + fiscalCode +" " + mail +" " + phone + " " + password +" " +role);
        try {
            return u.registerUser(name, surname, fiscalCode, mail, phone, password, role);
        } catch (IOException e) {
            e.printStackTrace();
            output = "ERROR: " + e.toString();
        }
        return output;
    }


    @Override
    public void onPostExecute(String result) {
        super.onPostExecute(result);
        Toast.makeText(weakContext.get(), result, Toast.LENGTH_LONG).show();
        if(!result.contains("ERROR")) {
            Intent loginActivity = new Intent(weakContext.get(), LoginActivity.class);
            loginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            weakContext.get().startActivity(loginActivity);
        }
    }
}

